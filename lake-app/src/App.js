import React from 'react';

const App = ({ lakes }) => (
  <div>
    {lakes.map(lake => <li key={lake.id}>{lake.name} | Trailhead: {lake.trailhead}</li>)}
  </div>
);

export default App;
