learningReactJs4

# Instalando o React Developer Tools

Buscar por "React Developer Tools" no Google Chrome extension

Depois de instalado entrar no site https://moonhighway.com. O ícono do plugin será habilitado.

# Create React App

https://facebook.github.io/create-react-app

```
$ npm i create-react-app -g
```

Para criar um projeto chamado lake-app:

```
$ create-react-app lake-app
```

# Build

Quando executamos o `npm start` do projeto criado pelo create-react-app, ele nos exibe essa mensagem:

**Note that the development build is not optimized.
To create a production build, use `yarn build`.**

Execute o `yarn build` para gerar o build para produção

Após executar o comando ele exibe esse texto:


> The project was built assuming it is hosted at `the server root`. 

>You can control this with the `homepage` field in your `package.json`. 
For example, add this to build it for GitHub Pages:

```json
  "homepage" : "http://myname.github.io/myapp",
```

>The build folder is ready to be deployed.
>You may serve it with a static server:

```
  $ yarn global add serve
  $ serve -s build
```
>Find out more about deployment here:
>bit.ly/CRA-deploy
  
# Learn more

https://reactjs.org

